var path = require('path')
var ExtractText = require('extract-text-webpack-plugin')
var WebpackHTML = require('html-webpack-plugin')

module.exports = {
    entry: [
        './src/main.ts'
    ],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    devtool: 'inline-source-map',
    plugins:[
        new ExtractText('styles.css'),
        new WebpackHTML({
            template: './src/index.html'
        })
    ],
    resolve:{
        extensions: ['.ts','.js']
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractText.extract({
                    use: ['css-loader']
                })
            },
            {
                test: /\.scss$/,
                use: ExtractText.extract({
                    use: ['css-loader','sass-loader']
                })
            },
            {
                test: /\.ts$/,
                use: 'awesome-typescript-loader',
                exclude: /node_modules/
            },
            // {
            //     test: /\.js$/,
            //     use: 'babel-loader',
            //     exclude: /node_modules/
            // },
            {
                test: /\.(jpg|png|gif|svg)$/,
                use:{
                    loader: 'file-loader',
                    options:{
                        name: 'images/[name]-[hash].[ext]'
                    }
                }
            }
        ]
    }
}