# Zamieniamy pliki na moduly CommonJS (node):
- exportujemy symbole 
    module.exports = App
- importujemy symbole
    var app = require('./app.js')

# Mozemy je uruchomic w node
node ./src/main.js

# Ale nie w przegladarce
# Instalujemy webpack - paker modulow
npm install webpack

# Dodajemy do scripts w package.json
"scripts":{
    "build":"webpack ./src/main.js ./dist.main.js"
}

# Pakujemy moduly
npm run build

# Zmieniamy w indexie pliki na nasz bundle.js
<script src="bundle.js"></script>

# Odswwierzamy w przegladarce