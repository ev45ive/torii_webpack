# Wchodzimy na strone repozytorium:
https://bitbucket.org/ev45ive/torii_webpack/

# wybieramy gdzie pobrac katalog
cd ..

# Wybieramy HTTPS i kopiujemy polecenie GIT
git clone https://ev45ive@bitbucket.org/ev45ive/torii_webpack.git moj_webpack

# Wchodzimy do pobranego katalogu 
cd moj_webpack

# Instalujemy biblioteki i narzedzia z pliku package.json
npm install

# Budujemy aplikacje poleceniem
npm run build

# lub

# Uruchamiamy serwer developerski poleceniem
npm run serve

# Uruchamiamy przegladarke na adresie http://localhost:8080/
http://localhost:8080/