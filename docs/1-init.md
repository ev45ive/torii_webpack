# Inicjalizacja Projektu
npm init -y

# Powstaje package.json

# Instalacja Serwera Plikow
npm install http-server

# Dodajemy polecenie do sekcji "scripts" w package.json :
"scripts:{
    "start" : "http-server"
    ...

# Uruchamiamy serwer
npm run start

# Uruchamiamy przegladarke na stronie
http://localhost:8080/

# Git
git init
echo "node_modules" > .gitignore
git status
git add .
git commit -m "Pierwszy commit"