HTML
CSS
- Bootstrap CSS

JavaScript (ES5)
- jQuery
- Lodash / Underscore
- Backbone (View,Model)

Node Package Manager (NPM)
- package.json

Webpack (module loader / bundler )
- Moduly Node (CommonJS)
- Babel

------------

EcmaScript6 (EcmaScript2015)
TypeScript ( Silne Typowanie + ES6 )

Karma + Protractor
- Jasmine

Angular4
- Change Detection
- ShadowDOM
- Dependency Injection
- Services
- RxJS
- * Redux / Immutable

--- BACKEND ---
Node
- ExpressJS
*MongoDB
*MySQL