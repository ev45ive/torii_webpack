
export interface IProduct{
    id:number;
    name:string;
    value:number;
    type?:PRODUCT_TYPE
    discount?: number
};

export enum PRODUCT_TYPE {
    /**
     * Products that need to be sent
     */
    PHISICAL,
    DIGITAL,
}

export const StringQuery = query => ({
    query,
    check({name}){
        return name.indexOf(this.query) != -1
    }
})

export const MinPriceQuery = minprice => ({
    minprice,
    check({value}){
        return value >= this.minprice
    }
})

const TAX = 1.23;

export class Product implements IProduct{
  
    constructor(
        public id:number,
        public name:string, 
        public value:number){
    }


    getPrice(){
        return this.value * TAX
    }

}

export const OPTIONS = {

}