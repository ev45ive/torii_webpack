//import { uniqBy } from 'lodash'
import {IProduct, Product} from './product'

export interface ProductQuery{
    check(product:IProduct):boolean
}

export class Products{
    private products:IProduct[] = []

    constructor(){
       
    }

    getProducts(){
        return this.products
    }
    
    search(query:ProductQuery){
        return this.products.filter(
            product => query.check(product)
        )
    }

    addProduct(product:IProduct){
        product = new Product(product.id,product.name,product.value)
        this.products.push(product)
    }

    loadProducts(products:IProduct[]){
        products.forEach(product =>{
            this.addProduct(product)
        })
    }

}