import { 
    Product,
    IProduct,
    PRODUCT_TYPE,
    MinPriceQuery,
    StringQuery,
    OPTIONS as settings 
} from './products/product'

import { Products, ProductQuery } from './products/products'

import { products as fixture} from './products/fixtures'

import { Cart } from './order/cart'

class App{
    version = '4.2.5'
    
    constructor(){
    }

    run(){

        const catalog = new Products();
        catalog.loadProducts(fixture);

        let p = fixture[0];
        (<IProduct>p).type = PRODUCT_TYPE.PHISICAL;

        console.log(PRODUCT_TYPE[p['type']])


        window['catalog'] = catalog;   
        
        var found = (catalog.search({
            check(product:IProduct){
                return [
                    MinPriceQuery(24),
                    StringQuery('Ali')
                ].every((query:ProductQuery) => {
                    return query.check(product)
                })
            }
        }))

    
        console.log('Version ' + this.version)
    }
}
export default App;
