
function add(a,b){ return a+b }

function divide(a,b){ 
    if(b == 0){
        throw Error('Zero division');
    }

    return a/b 
}

function form(fields){
   return fields.reduce(function(result,field){
        return add(result,field)
   },0)
}

function superexperimental(){
    return Infinity / Infinity
}