
describe('Tests', function(){

    describe('Calc', function(){

        it('should add numbers',function(){
            expect(add(1,2)).toEqual( 3 )
        })

        
        it('should divide numbers',function(){
            expect(divide(4,2)).toEqual( 2 )
            expect(divide(4,-2)).toEqual( -2 )
        })

        it('should not divde by zero',function(){            
            expect(function(){
                divide(4,0)
            }).toThrow()
        })
    })

    describe('Form',function(){
        it('should add form fields',function(){
            expect(form([1,2])).toEqual(3)
        })
    })

    describe('Truths', function(){

        it('should test true is true',function(){
            expect(true).toEqual(true)
        })
    })

})