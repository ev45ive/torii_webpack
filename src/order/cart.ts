import {IProduct} from '../products/product'

export class Cart{

    items = []

    addProduct(product:IProduct){
        // find exiting position
        let item = this.items.find(
             ({product_id}) => product.id == product_id
        )
        // add new position
        if(!item){
            item = {
                product_id: product.id,
                product,
                count:0
            }
            this.items.push(item)
        }
        // update position
        item.count++;
        item.subtotal = this.calculateSubtotal(item)
    }

    private calculateSubtotal({product, count}){
        let discount = product.discount || 0;
        return product.value * count * (1-discount);
    }

    calculateTotal(){
        let total = this.items.reduce( (sum,item)=>{
            return sum + item.subtotal
        },0) ;
        return this._calculateDiscount(total)
    }

    protected _calculateDiscount(total){
        return total * (1 - this.discount)
    }

    getItems(){
        return this.items
    }

    discount = 0;

    setDiscount(discount){
        this.discount = discount;
    }
}