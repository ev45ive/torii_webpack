import { Cart } from './Cart';
import { products as fixture} from '../products/fixtures';

describe('Cart', () => {
    let cart;

    beforeEach(()=>{
       cart = new Cart()
    })

    it('should add products',()=>{     
       cart = new Cart()   
       expect(cart.getItems().length).toEqual(0)
       cart.addProduct(fixture[0])

       var items = cart.getItems()
       expect(items.length).toEqual(1)
    })


    it('should calculate multiple item', () => {
       cart = new Cart()
       
        cart.addProduct(fixture[0])
        cart.addProduct(fixture[1])
        cart.addProduct(fixture[2])
        expect(cart.calculateTotal()).toEqual(200)

        cart.addProduct(fixture[0])
        expect(cart.calculateTotal()).toEqual(300)
    })

    describe('Discounts ', () => {
        it('should calculate global discount',()=>{
            const discount = 0.10
            cart.setDiscount(discount)
            cart.addProduct({id:123, name:'123',value:100})
            expect(cart.calculateTotal()).toEqual( 90 )
        })

        it('should calculate product discount',()=>{
            const discount = 0.10
            cart.addProduct({
                value:100,
                discount
            })
            expect(cart.calculateTotal()).toEqual( 90 )
        })

        it('should calculate product amount discount',()=>{

        })

        it('should calculate discount over total sum',()=>{

        })
    })
})